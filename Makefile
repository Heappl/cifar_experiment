all:
	$(MAKE) -C liblinear
	$(MAKE) -C libsvm
	mkdir -p cifar10_data/build
	cd cifar10_data/build && cmake -DCMAKE_BUILD_TYPE=Release ..
	$(MAKE) -C cifar10_data/build

