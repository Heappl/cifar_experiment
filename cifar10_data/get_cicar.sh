#!/bin/bash

wget --no-check-certificate https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz
tar xf cifar-10-binary.tar.gz
rm cifar-10-binary.tar.gz
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release  ..
make
cd -

mkdir -p output_227
build/extract_cifar output/data_ 227 cifar-10-batches-bin/data_batch_*
build/extract_cifar output/test_ 227 cifar-10-batches-bin/test_batch.bin

