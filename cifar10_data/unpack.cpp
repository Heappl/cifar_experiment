#include <cstdlib>
#include <iostream>
#include <ostream>
#include <vector>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <thread>
#include <future>

using ImageData = std::vector<char*>;
void read_bin(ImageData& data, const char* filename, int size)
{
    auto ptr = fopen(filename, "rb");
    fseek (ptr , 0 , SEEK_END);
    auto totalSize = ftell(ptr);
    rewind(ptr);
    char* buffer = new char[totalSize];
    auto read = fread(buffer, totalSize, 1, ptr);
    if (read == 0)
    {
        std::cout << "ERROR, nothing read" << std::endl;
        return;
    }

    for (int i = 0; i < totalSize; i += size)
        data.push_back(buffer + i);
    fclose(ptr);
}

struct MeanData
{
    struct Mean
    {
        double sum = 0;
        long long count = 0;

        void append(int val) { sum += val; ++count; }
        double get() const { return sum / count; }
    } red, green, blue;
};

void create_images(std::vector<std::string>& descr, MeanData& means, const std::string& prefix, ImageData& data, int size, int newSize)
{
    std::vector<int> compression_params = {CV_IMWRITE_PNG_COMPRESSION, 9};
    for (auto image : data)
    {
        cv::Mat mat(newSize, newSize, CV_8UC3);
        auto get_class = [&]{ return int(image[0]); };
        auto get_new = [&](int arg) { return (arg * size) / newSize; };
        auto get = [&](char* buffer, int row, int col) { return (uint8_t)buffer[get_new(row) * size + get_new(col)]; };
        auto get_red = [&](int row, int col) { return get(image + 1, row, col); };
        auto get_green = [&](int row, int col) { return get(image + 1 + size * size, row, col); };
        auto get_blue = [&](int row, int col) { return get(image + 1 + 2 * size * size, row, col); };
        for (int r = 0; r < newSize; ++r)
            for (int c = 0; c < newSize; ++c)
            {
                cv::Vec3b& rgb = mat.at<cv::Vec3b>(r, c);
                rgb[0] = get_red(r, c);
                rgb[1] = get_green(r, c);
                rgb[2] = get_blue(r, c);
                means.red.append(get_red(r, c));
                means.green.append(get_green(r, c));
                means.blue.append(get_blue(r, c));
            }
        if (descr.size() % 1000 == 0)
            std::cout << "wrote " << descr.size() << " images" << std::endl;
        auto filename = prefix + std::to_string(get_class()) + "_" + std::to_string(descr.size()) + ".png";
        imwrite(filename.c_str(), mat, compression_params);
        descr.push_back(filename + " " + std::to_string(get_class()));
    }
}

void convert(std::vector<std::string>& descr, MeanData& means, const std::string& prefix, const std::string& name, size_t newSize)
try
{
    ImageData data;
    read_bin(data, name.c_str(), 3 * 32 * 32 + 1);
    std::cout << "read " << data.size() << " images from " << name << std::endl;
    create_images(descr, means, prefix, data, 32, newSize);
}
catch (std::exception const& error)
{
    std::cerr << "ERROR: " << error.what() << std::endl;
}

int main(int argc, char* argv[])
{
    std::vector<std::string> args;
    std::string prefix = argv[1];
    int newSize = atoi(argv[2]);
    for (int i = 3; i < argc; ++i)
        args.push_back(argv[i]);
    std::vector<std::string> descr;
    MeanData means;
    for (auto& file : args)
        convert(descr, means, prefix, file, newSize);
    std::cout << "Means: " << means.red.get() << ", " << means.green.get() << ", " << means.blue.get() << std::endl;
    std::ofstream of(prefix + std::string("images.txt"));
    for (auto& line : descr)
        of << line << std::endl;
    of.close();
    return 0;
}
