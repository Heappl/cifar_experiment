import os, sys, tarfile
from six.moves import urllib
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import pickle
import tensorflow as tf
from dataimport import maybe_download_and_extract, load_cifar, dump_for_liblinear
from scipy.misc import imresize as imresize
from svm import svm as svm
from itertools import islice

MODEL_DIR = 'model'
DATA_DIR = 'data'

#from Tensorflow classify_image.py
def create_graph(path):
    """Creates a graph from saved GraphDef file and returns a saver."""
    with tf.gfile.FastGFile(path, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        tf.import_graph_def(graph_def, name='')

def zoom_single(img, times):
    return imresize(img, times)
def zoom_all(data, times):
    return np.array([zoom_single(img, times) for img in data])

def extract_codes(path, inlayer, outlayer, data, labels, first_n = None, batch = 5, zoom = 1.0):
    tf.reset_default_graph()
    create_graph(path)
    ret = []
    if first_n != None:
        data = np.array(list(islice(data, first_n)))
        labels = np.array(list(islice(labels, first_n)))
    with tf.Session() as sess:
        pool = sess.graph.get_tensor_by_name(outlayer + ':0')
        input = sess.graph.get_tensor_by_name(inlayer + ':0')
        total = (data.shape[0] + batch - 1) // batch
        if input.shape and (len(input.shape) == 3):
            for img in data:
                if zoom != 1.0:
                    img = zoom_single(img, zoom)
                ret.append(sess.run(pool, {inlayer + ':0': img}))
                if (len(ret) % batch == 0):
                    sys.stdout.write("\r>> iteration: %i/%i" % (len(ret) // batch, total))
        else: #try batch 
            for i in range(0, total):
                curr = np.array(list(islice(data, i * batch, (i + 1) * batch)))
                if zoom != 1.0:
                    curr = zoom_all(curr, zoom)
                out = sess.run(pool, {inlayer + ':0': curr})
                ret.append(out)
                sys.stdout.write("\r>> iteration: %i/%i" % (i + 1, total))
        print(" finished codes extraction for %s" % path)
    return (np.concatenate(ret), np.array(labels))

def load_or_extract_codes(name, inlayer, outlayer, output_name, data, labels, zoom = 1.0):
    codes_path = os.path.join(DATA_DIR, output_name + ".data.npy")
    labels_path = os.path.join(DATA_DIR, output_name + ".labels.npy")
    if not os.path.exists(codes_path):
        (codes, labels) = extract_codes(os.path.join(MODEL_DIR, name), inlayer, outlayer, data, labels, zoom=zoom)
        np.save(codes_path, codes, allow_pickle=True)
        np.save(labels_path, labels, allow_pickle=True)
    else:
        codes = np.load(codes_path, allow_pickle=True)
        labels = np.load(labels_path, allow_pickle=True)
    return (codes, labels)

def load_or_extract_all_codes(name, inlayer, outlayer, data, test, infix="", zoom = 1.0):
    codes_path = os.path.join(DATA_DIR, name + '.codes')
    data = load_or_extract_codes(name, inlayer, outlayer, name + infix + ".data", data[0], data[1], zoom=zoom)
    test = load_or_extract_codes(name, inlayer, outlayer, name + infix + ".test", test[0], test[1], zoom=zoom)
    return (data, test)

def print_layers(name):
    tf.reset_default_graph()
    create_graph(os.path.join(MODEL_DIR, name))
    with tf.Session() as sess:
        for op in sess.graph.get_operations():
            print(op.name)
    sys.exit(0)

maybe_download_and_extract(MODEL_DIR, 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz')
maybe_download_and_extract(MODEL_DIR, 'https://deepdetect.com/models/tf/vgg_19/vgg_19.pb', extract=False)
maybe_download_and_extract(MODEL_DIR, 'https://deepdetect.com/models/tf/resnet_v1_152/resnet_v1_152.pb', extract=False)

(data, test) = load_cifar()
print("loaded cifar data")

res_codes = load_or_extract_all_codes('resnet_v1_152.pb', 'InputImage', 'resnet_v1_152/pool5', data, test, zoom = 7.0)
#vgg_codes = load_or_extract_all_codes('vgg_19.pb', 'InputImage', 'vgg_19/fc8/squeezed', data, test, zoom=7.0)
vgg_codes = load_or_extract_all_codes('vgg_19.pb', 'InputImage', 'vgg_19/pool5/MaxPool', data, test, infix=".3")
incept_v3_codes = load_or_extract_all_codes('classify_image_graph_def.pb', 'DecodeJpeg', 'pool_3', data, test)
print("loaded codes")

print("EQUAL", np.array_equal(res_codes[0][1], vgg_codes[0][1]))
print("EQUAL", np.array_equal(incept_v3_codes[0][1], vgg_codes[0][1]))
print("EQUAL", np.array_equal(incept_v3_codes[0][1], res_codes[0][1]))
print("EQUAL", np.array_equal(res_codes[1][1], vgg_codes[1][1]))
print("EQUAL", np.array_equal(incept_v3_codes[1][1], vgg_codes[1][1]))
print("EQUAL", np.array_equal(incept_v3_codes[1][1], res_codes[1][1]))

#train_file = dump_for_liblinear(vgg_codes[0][0], vgg_codes[0][1], "./vgg_data_codes.txt")
#test_file = dump_for_liblinear(vgg_codes[1][0], vgg_codes[1][1], "./vgg_test_codes.txt")
#train_file = dump_for_liblinear(vgg_codes[0][0], vgg_codes[0][1], "./vgg_data_codes_relu.txt")
#test_file = dump_for_liblinear(vgg_codes[1][0], vgg_codes[1][1], "./vgg_test_codes_relu.txt")
#train_file = dump_for_liblinear(incept_v3_codes[0][0], incept_v3_codes[0][1], "./incept_v3_data_codes.txt")
#test_file = dump_for_liblinear(incept_v3_codes[1][0], incept_v3_codes[1][1], "./incept_v3_test_codes.txt")
train_file = dump_for_liblinear(res_codes[0][0], res_codes[0][1], "./res_data_codes.txt")
test_file = dump_for_liblinear(res_codes[1][0], res_codes[1][1], "./res_test_codes.txt")

svm(train_file, test_file, "resnet", "radial")
sys.exit(0)

#svm(train_file, test_file, "vgg19_next_to_last", "linear")
#sys.exit(0)

def merge_codes(codes_list):
    ret = []
    for i in range(0, len(codes_list[0])):
        aux = np.concatenate([list[i].reshape(list[i].shape[-1]) for list in codes_list])
        ret.append(aux)
    return ret

train_data_all = merge_codes([incept_v3_codes[0][0], vgg_codes[0][0], res_codes[0][0]])
test_data_all = merge_codes([incept_v3_codes[1][0], vgg_codes[1][0], res_codes[1][0]])
print("merged")

#train_file = dump_for_liblinear(train_data_all, res_codes[0][1], "all_data_codes.txt")
#print("dumped train")
#test_file = dump_for_liblinear(test_data_all, res_codes[1][1], "all_test_codes.txt")
#print("dumped test")

#svm(train_file, test_file, "multi", "linear")
    

def train_nn_with_codes(codes, labels, test_codes, test_labels, batch = 100):
    tf.reset_default_graph()
    total_size = sum(list(codes[0].shape))
    data_size = len(codes)

    x = tf.placeholder(tf.float32, shape=[batch, total_size])
    y = tf.placeholder(tf.int32, shape=[batch,])
    y_ = tf.one_hot(y, 10)

    def weight_variable(shape):
      initial = tf.truncated_normal(shape, stddev=0.1)
      return tf.Variable(initial)

    def bias_variable(shape):
      initial = tf.constant(0.1, shape=shape)
      return tf.Variable(initial)
    
    
    W_fc1 = weight_variable([total_size, 4096])
    b_fc1 = bias_variable([4096])
    h_fc1 = tf.nn.relu(tf.matmul(x, W_fc1) + b_fc1)
    
    W_fc2 = weight_variable([4096, 1024])
    b_fc2 = bias_variable([1024])
    h_fc2 = tf.nn.relu(tf.matmul(h_fc1, W_fc2) + b_fc2)

    W_fc3 = weight_variable([1024, 10])
    b_fc3 = bias_variable([10])
    out = tf.matmul(h_fc2, W_fc3) + b_fc3
    
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=out))
    train_step = tf.train.GradientDescentOptimizer(1e-4).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(out, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
    def get_slice(data, n, batch):
        data_size = len(data)
        slice_begin = (n * batch) % data_size
        slice_end = ((n + 1) * batch) % data_size
        if slice_end < slice_begin:
            curr1 = list(islice(data, slice_begin, data_size))
            curr2 = list(islice(data, slice_end))
            return np.array(curr1 + curr2)
        else:
            return np.array(list(islice(data, slice_begin, slice_end)))

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        iters = 200000
        for i in range(0, iters):
            curr = get_slice(codes, i, batch)
            curr_labels = get_slice(labels, i, batch)
            sys.stdout.write("\r>> iteration: %i/%i (%s)" % (i + 1, iters, str((curr.shape, curr_labels.shape))))
            out = train_step.run(feed_dict={x: curr, y: curr_labels})
            if ((i + 1) % 1000 == 0):
                acc = 0
                batches = len(test_codes) // batch
                for j in range(0, batches):
                    acc += accuracy.eval(feed_dict={x: get_slice(test_codes, j, batch), y: get_slice(test_labels, j, batch)})
                print("")
                print("accuracy: %f" % (acc / batches))

train_nn_with_codes(train_data_all, incept_v3_codes[0][1], test_data_all, incept_v3_codes[1][1])
        
        
