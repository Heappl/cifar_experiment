from cv2 import HOGDescriptor, imread
import numpy as np
from sklearn import svm
from dataimport import maybe_download_and_extract, load_cifar, dump_for_liblinear, DATA_DIR
import matplotlib.pyplot as plt
import sys, os
from svm import svm as svm

def extract_oriented_gradients(data):
    ret = []
    for img in data:
        hog = HOGDescriptor((32, 32), (8, 8), (4, 4), (4, 4), 3)
        grads = hog.compute(img)
        ret.append(grads.reshape(grads.shape[0]))
        if (len(ret) % 1000 == 0):
            sys.stdout.write('\r extracted %i/%i' % (len(ret), data.shape[0]))
    print(" finished")
    return np.array(ret)

(data, test) = load_cifar()

grads = extract_oriented_gradients(data[0])
test_grads = extract_oriented_gradients(test[0])

train_file = dump_for_liblinear(grads, data[1], "shallow_train.txt")
test_file = dump_for_liblinear(test_grads, test[1], "shallow_test.txt")

svm(train_file, test_file, "shallow", "linear")
#svm(train_file, test_file, "shallow", "radial")


