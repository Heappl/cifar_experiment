from PIL import Image
from keras.datasets import cifar10
from keras.backend import tf as ktf
import scipy
import numpy as np
import keras

num_classes = 10
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
#x_train = x_train.astype('float32')
#x_test = x_test.astype('float32')


def resize(x):
    new_shape = (160,160,3)
    ret = np.empty(shape=(x.shape[0],)+new_shape)
    for idx in range(x.shape[0]):
        ret[idx] = scipy.misc.imresize(x[idx], new_shape)
    return ret
def dump(x, name):
    new_img = Image.new("RGB", (x.shape[0], x.shape[1]), "white")
    lst = [tuple(p) for row in x for p in row ]
    new_img.putdata(lst)
    new_img.save(name)

def mprint(x):
    for r in range(0, x.shape[0]):
        aux = ""
        for c in range(0, x.shape[1]):
            aux += " " + str(x[r][c])
        print(aux)

#x_train = resize(x_train).astype('float32')

model = keras.applications.resnet50.ResNet50(include_top=False, weights='imagenet', classes=10)
opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

#model.fit(x_train, y_train, epochs=5, batch_size=32)

x_test = resize(x_test).astype('float32') / 255
print(model.predict(x_test))

