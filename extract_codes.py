import os, sys, tarfile
from six.moves import urllib
import numpy as np
import tensorflow as tf
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import pickle
from dataimport import load_cifar, maybe_download_and_extract, just_unpickle

#from Tensorflow classify_image.py
MODEL_DIR = 'model'
DATA_DIR = 'data'
MODEL_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'
DATA_URL = 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'
def create_graph():
  """Creates a graph from saved GraphDef file and returns a saver."""
  # Creates graph from saved graph_def.pb.
  with tf.gfile.FastGFile(os.path.join(MODEL_DIR, 'classify_image_graph_def.pb'), 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

def extract_codes(data, labels, first_n = None):
    create_graph()
    ret = []
    first_n = min(first_n, len(data))
    from itertools import islice
    with tf.Session() as sess:
        pool = sess.graph.get_tensor_by_name('pool_3:0')
        #for op in sess.graph.get_operations():
            #print(op.name)
        for (img, label) in islice(zip(data, labels), first_n):
            ret.append((label, sess.run(pool, {'DecodeJpeg:0': img})))
            if (len(ret) % 1000 == 0):
                print("calculated %i/%i" % (len(ret), data.shape[0]))
    return ret

def dump_for_liblinear(codes, output):
    with open(output, 'w') as out:
        for (l, c) in codes:
            line = str(l)
            for i in range(0, c.shape[-1] + 1):
                line += " " + str(i + 1) + ":" + str(c[0][0][0][i - 1])
            out.write(line + "\n")

def get_mins_and_maxes(data):
    return [min([x[0] for x in data]),
            max([x[0] for x in data]) + 1,
            min([x[1] for x in data]),
            max([x[1] for x in data]) + 1]

def plot_classes(data, labels):
    from random import shuffle
    classes = {}
    zipped = list(zip(data, labels))
    shuffle(zipped)
    for (img, label) in zipped:
        if label not in classes:
            classes[label] = []
        if len(classes[label]) < 10:
            classes[label].append(img)
    plt.figure(figsize=(10, 10))
    row = 0
    fig, ax = plt.subplots()
    names = [n.decode() for n in just_unpickle('data/cifar-10-batches-py/batches.meta')[b'label_names']]
    for k in classes.keys():
        col = 0
        for img in classes[k]:
            im = OffsetImage(img, zoom=2.0)
            ab = AnnotationBbox(im, (col+.5, row+.5), xycoords='data', frameon=False)
            ax.add_artist(ab)
            col += 1
        row += 1
    ax.set_ylim(0, 10)
    ax.set_xlim(0, 10)
    plt.yticks(list(range(0, 10)), names)
    plt.show()
    

def plot_tsne(codes, imgs=None):

    flattened = np.array([c.reshape((c[0].shape[-1])) for (l, c) in codes])
    labels = [l for (l, c) in codes]
    X_embedded = TSNE(n_components=2, n_iter=1000).fit_transform(flattened)
    plt.figure(figsize=(30, 30))
    fig, ax = plt.subplots()
    for x, i in zip(X_embedded, range(0, len(X_embedded))):
        if imgs is not None:
            im = OffsetImage(imgs[i], zoom=0.75)
            ab = AnnotationBbox(im, (x[0]+.5, x[1]+.5), xycoords='data', frameon=False)
            ax.add_artist(ab)
        else:
            plt. text(x[0]+.5,  x[1]+.5,  str(labels[i]),
                      color=plt.cm.Dark2(labels[i] / 4.), fontdict={'weight': 'bold',  'size': 11})
    plt.axis(get_mins_and_maxes(X_embedded))
    fig.tight_layout()
    plt.show()

def plot_som(codes):
    from minisom import MiniSom as MiniSom

    flattened = np.array([c.reshape((c[0].shape[-1])) for (l, c) in codes])
    labels = [l for (l, c) in codes]
    som = MiniSom(30, 30, flattened.shape[-1], sigma=0.3, learning_rate=0.5) # initialization of 6x6 SOM
    som.train_batch(flattened, 3000)

    plt.figure(figsize=(10, 10))
    im = 0
    points = []
    for x, t in zip(flattened, labels):  # scatterplot
        w = som.winner(x)
        points.append((w[0], w[1]))
        plt.text(w[0]+.5,  w[1]+.5,  str(t),
                  color=plt.cm.Dark2(t / 4.), fontdict={'weight': 'bold',  'size': 11})
    plt.axis(get_mins_and_maxes(points))
    plt.show()


maybe_download_and_extract(MODEL_DIR, MODEL_URL)
(data, test) = load_cifar()
#plot_classes(data[0], data[1])
#plot_tsne(extract_codes(test[0], test[1], 1000), test[0])
plot_som(extract_codes(test[0], test[1], 300))
#dump_for_liblinear(codes, "./data_codes_for_linear.txt")
#dump_for_liblinear(test_codes, "./test_codes_for_linear.txt")

