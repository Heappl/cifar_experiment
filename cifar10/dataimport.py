import os, sys, tarfile
from six.moves import urllib
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import pickle

DATA_DIR = 'data'

#from Tensorflow classify_image.py
def maybe_download_and_extract(dir, url, extract=True):
    """Download and extract model tar file."""
    if not os.path.exists(dir):
        os.makedirs(dir)
    filename = url.split('/')[-1]
    filepath = os.path.join(dir, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
          sys.stdout.write('\r>> Downloading %s %.1f%%' % (
              filename, float(count * block_size) / float(total_size) * 100.0))
          sys.stdout.flush()
        filepath, _ = urllib.request.urlretrieve(url, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
        if extract:
            tarfile.open(filepath, 'r:gz').extractall(dir)

#from https://www.cs.toronto.edu/~kriz/cifar.html
def just_unpickle(file):
    with open(file, 'rb') as fo:
        return pickle.load(fo, encoding='bytes')
def unpickle(file, no_transpose):
    dict = just_unpickle(file)
    data = np.array(dict[b'data']).reshape(10000, 3, 32, 32)
    if not no_transpose:
        data = data.transpose(0, 2, 3, 1)
    return (data, np.array(dict[b'labels']))

def load_cifar(data_only=False, no_transpose=False):
    maybe_download_and_extract(DATA_DIR, 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz')
    dir = os.path.join(DATA_DIR, "cifar-10-batches-py")
    data = [unpickle(os.path.join(dir, "data_batch_" + str(i)), no_transpose) for i in range(1, 6)]
    data = (np.concatenate([x[0] for x in data]), np.concatenate([x[1] for x in data]))
    if data_only:
        return (data, np.array([]))
    test = unpickle(os.path.join(dir, "test_batch"), no_transpose)
    return (data, test)

def dump_for_liblinear(codes, labels, output):
    path = os.path.join(DATA_DIR, output)
    def prod(list):
        ret = 1
        for elem in list:
            ret *= elem
        return ret
    lines = []
    for (l, c) in zip(labels, codes):
        line = str(l)
        aux = c.reshape((prod(list(c.shape))))
        for i in range(0, aux.shape[0] + 1):
            line += " " + str(i + 1) + ":" + str(aux[i - 1])
        lines.append(line)
    with open(path, 'w') as out:
        for line in lines:
            out.write(line + "\n")
    return path
