import os, sys, tarfile
from six.moves import urllib
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import pickle
import tensorflow as tf

DATA_DIR = 'data'

#from Tensorflow classify_image.py
def maybe_download_and_extract(dir, url, extract=True):
    """Download and extract model tar file."""
    if not os.path.exists(dir):
        os.makedirs(dir)
    filename = url.split('/')[-1]
    filepath = os.path.join(dir, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
          sys.stdout.write('\r>> Downloading %s %.1f%%' % (
              filename, float(count * block_size) / float(total_size) * 100.0))
          sys.stdout.flush()
        filepath, _ = urllib.request.urlretrieve(url, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
        if extract:
            tarfile.open(filepath, 'r:gz').extractall(dir)

#from https://www.cs.toronto.edu/~kriz/cifar.html
def just_unpickle(file):
    with open(file, 'rb') as fo:
        return pickle.load(fo, encoding='bytes')
def unpickle(file):
    dict = just_unpickle(file)
    data = np.array(dict[b'data']).reshape(10000, 3, 32, 32).transpose(0, 2, 3, 1)
    return (data, np.array(dict[b'labels']))

def load_cifar(dir):
    data = [unpickle(os.path.join(dir, "data_batch_" + str(i))) for i in range(1, 6)]
    data = (np.concatenate([x[0] for x in data]), np.concatenate([x[1] for x in data]))
    test = unpickle(os.path.join(dir, "test_batch"))
    return (data, test)

