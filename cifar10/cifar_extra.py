import cifar10
import tensorflow as tf

import os, sys, tarfile
from six.moves import urllib
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import pickle
import tensorflow as tf
from dataimport import maybe_download_and_extract, load_cifar, dump_for_liblinear
from scipy.misc import imresize as imresize
from svm import svm as svm

MODEL_DIR = 'model'
DATA_DIR = 'data'

def zoom_single(img, times):
    return imresize(img, times)
def zoom_all(data, times):
    return np.array([zoom_single(img, times) for img in data])

def extract_codes(inlayer, outlayer, data, plabels, first_n = None, batch = 128, zoom = 75):
    with tf.Graph().as_default() as g:
        images, labels = cifar10.inputs(eval_data=True)
        images = tf.placeholder(tf.float32, [batch, 24, 24, 3])
        labels = tf.placeholder(tf.float32, shape=[batch, 10])
        logits = cifar10.inference(images)
        # Restore the moving average version of the learned variables for eval.
        variable_averages = tf.train.ExponentialMovingAverage(
            cifar10.MOVING_AVERAGE_DECAY)
        variables_to_restore = variable_averages.variables_to_restore()
        saver = tf.train.Saver(variables_to_restore)
        from itertools import islice
        ret = []
        if first_n != None:
            data = np.array(list(islice(data, first_n)))
            plabels = np.array(list(islice(plabels, first_n)))
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state("models")
            saver.restore(sess, ckpt.model_checkpoint_path)
            for op in g.get_operations():
                print(op.name)

            total = (data.shape[0] + batch - 1) // batch
            for i in range(0, total):
                curr = np.array(list(islice(data, i * batch, (i + 1) * batch)))
                if (len(curr) < batch):
                    curr = np.concatenate([curr, np.array(list(islice(data, batch - len(curr))))])
                if zoom != 1.0:
                    curr = zoom_all(curr, zoom)
                out = sess.run(logits, {images: curr})
                ret.append(out)
                sys.stdout.write("\r>> iteration: %i/%i" % (i + 1, total))
        print(" finished")
        return (np.concatenate(ret), np.array(plabels))


def main(argv=None):
    (data, test) = load_cifar()
    print("loaded cifar data")

    codes = extract_codes("conv1/conv1", "local4/local4", data[0], data[1])
    test_codes = extract_codes("conv1/conv1", "local4/local4", test[0], test[1])
    train_file = dump_for_liblinear(codes[0], data[1], "./cifar_data_codes.txt")
    test_file = dump_for_liblinear(test_codes[0], test[1], "./cifar_test_codes.txt")
    #svm(train_file, test_file, "cifar", "linear")
    #svm(train_file, test_file, "cifar", "radial")

tf.app.run()

