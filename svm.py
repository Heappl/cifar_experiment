from dataimport import DATA_DIR
from subprocess import call
import os

def svm(train_file, test_file, prefix, type):
    if type not in ['linear', 'radial']:
        raise Exception("Invalid type: " + str(type))
    linear_model = os.path.join(DATA_DIR, prefix + "_" + type + "_model")
    linear_out = os.path.join(DATA_DIR, prefix + "_" + type + "_out")
    command_prefix = "liblinear/"
    if (type == 'radial'):
        command_prefix = "libsvm/svm-"
    train_command = command_prefix + "train"
    test_command = command_prefix + "predict"
    call(['rm', linear_out])
    print(train_command, test_command)
    call([train_command, train_file, linear_model])
    call([test_command, test_file, linear_model, linear_out])

#clf = svm.SVC(kernel="linear") #rbf by default
#print("training svm")
#clf.fit(grads, data[1])

#print(clf.score(test_grads, test[1]))
